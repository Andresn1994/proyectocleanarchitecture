var app = angular.module('app', ["ngRoute"]);

function initMap() {
    let map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8
    });
}

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: 'src/modules/main-component/views/mainComponent.html',
            controller: 'mainController'
        })
        .when("/suma", {
            templateUrl: 'src/modules/suma-component/views/sumaComponent.html',
            controller: 'sumaController'
        });
});
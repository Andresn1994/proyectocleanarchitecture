app.controller('mainController', ['$scope', '$http', function ($scope, $http) {

  $scope.next = '!';
  $scope.gpsLatitud = 0;
  $scope.gpsLongitud = 0;
  $scope.stakLatitud = 0;
  $scope.stakLongitud = 0;
  $scope.apiLatitud = 0;
  $scope.apiLongitud = 0;
  $scope.apiName = '';
  $scope.IP = undefined;
  $scope.latitud = 0;
  $scope.longitud = 0;
  $scope.mapa = null;
  $scope.distancia1 = 0;
  $scope.distancia2 = 0;

  $scope.callIpStack = () => {
    return $http.post('http://api.ipstack.com/' + $scope.IP + '?access_key=475e51d2a5c1e491b71ac0a4d6a27680')
      .then(response => {
        $scope.stakLatitud = response.data.latitude;
        $scope.stakLongitud = response.data.longitude;
        $scope.apiName = 'Stack';
        L.marker([$scope.stakLatitud, $scope.stakLongitud]).addTo($scope.mapa)
          .bindPopup('Stack').openPopup();
      })
      .catch(error => console.log(error));
  }

  $scope.callIpApi = () => {
    return $http.post('http://ip-api.com/json/' + $scope.IP)
      .then(response => {
        $scope.apiLatitud = response.data.lat;
        $scope.apiLongitud = response.data.lon;
        L.marker([$scope.apiLatitud, $scope.apiLongitud]).addTo($scope.mapa)
          .bindPopup('IP-Api').openPopup();
      })
      .catch(error => console.log(error));
  }

  $scope.verMapa = () => {
    $scope.mapa = L.map('mapa').setView([
      -0.19143929999999998,
      -78.483356],
      13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYW5kcmVzbm9taW5hejkwODAiLCJhIjoiY2syY2VpZmx0MjgwOTNjbzBrNjAyaTQ4eiJ9.XWhUVOEpOi3sS1pEQuu_Nw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox.streets'
    }).addTo($scope.mapa);
  }

  $scope.getPublicIp = () => {
    return $http.get('https://api.ipify.org?format=json')
      .then(response => {
        console.log(response);
        $scope.IP = response.data.ip;
      })
      .catch(error => console.log(error));
  }

  $scope.callGeolocation = () => {
    navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeo);
  }

  let onSuccessGeolocation = function (position) {
    console.log(position)
    L.marker([position.coords.latitude, position.coords.longitude]).addTo($scope.mapa)
      .bindPopup('GPS').openPopup();
  };

  function onErrorGeo(error) {
    alert(error.message);
  }

  let rad = function(x) {
    return x * Math.PI / 180;
  };

  $scope.obtenerDistancia = (p1, p2) => {
    let R = 6378137; // Earth’s mean radius in meter
    let dLat = rad(p2.lat - p1.lat);
    let dLong = rad(p2.lng - p1.lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    console.log(d);
    return d;
  }
  
  $scope.getDistance = () => {
    let p1 = {
      lat: $scope.gpsLatitud,
      lng: $scope.gpsLongitud
    }
    let p2 = {
      lat: $scope.apiLatitud,
      lng: $scope.apiLongitud
    }
    let p3 = {
      lat: $scope.stakLatitud,
      lng: $scope.stakLongitud
    }
    $scope.distancia1 = $scope.obtenerDistancia(p1, p2);
    $scope.distancia2 = $scope.obtenerDistancia(p1, p3);
  };

  $scope.getPublicIp();
}]);
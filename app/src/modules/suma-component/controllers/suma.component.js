app.controller('sumaController', ['$scope', '$http', function ($scope, $http) {

  $scope.permisos = 'Permisos';
  $scope.gpsLatitud = '';
  $scope.gpsLongitud = '';
  $scope.apiLatitud = '';
  $scope.apiLongitud = '';
  $scope.apiName = '';
  $scope.IP = undefined;
  $scope.latitud = 0;
  $scope.longitud = 0;
  $scope.mymap = null;
  $scope.googleMap = null;
  $scope.mark1 = null;
  $scope.mark2 = null;
  $scope.distancia = 0;

  let onSuccessGeo = function (position) {
    $scope.gpsLatitud = position.coords.latitude;
    $scope.gpsLongitud = position.coords.longitude;
    let myLatLng = { lat: $scope.gpsLatitud, lng: $scope.gpsLongitud };
    $scope.googleMap = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 17
    });
    $scope.mark1 = new google.maps.Marker({
      position: myLatLng,
      map: $scope.googleMap,
      title: 'GPS!'
    });
  };

  function onErrorGeo(error) {
    alert(error.message);
  }

  $scope.callGoogleApi = () => {
    return $http({
      url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDtPVMtv5tmPP59Y0V0VnDeraTQhhacOZ0',
      method: "POST",
      data: {
        homeMobileCountryCode: 740
      }
    }).then(response => {
      $scope.apiLatitud = response.data.location.lat;
      $scope.apiLongitud = response.data.location.lng;
      let myltln = { lat: $scope.apiLatitud, lng: $scope.apiLongitud };
      $scope.mark2 = new google.maps.Marker({
        position: myltln,
        map: $scope.googleMap,
        title: 'Google-Api'
      });
    })
      .catch(error => console.log(error));
  }

  $scope.callGeolocation = () => {
    navigator.geolocation.getCurrentPosition(onSuccessGeolocation, onErrorGeo);
  }

  let onSuccessGeolocation = function (position) {
    console.log(position)
    $scope.gpsLatitud = position.coords.latitude;
    $scope.gpsLongitud = position.coords.longitude;
    let myLatLng = { lat: $scope.gpsLatitud, lng: $scope.gpsLongitud };
    $scope.mark1 = new google.maps.Marker({
      position: myLatLng,
      map: $scope.googleMap,
      title: 'GPS!'
    });
  };

  let rad = function(x) {
    return x * Math.PI / 180;
  };
  
  $scope.getDistance = () => {
    let p1 = {
      lat: $scope.gpsLatitud,
      lng: $scope.gpsLongitud
    }
    let p2 = {
      lat: $scope.apiLatitud,
      lng: $scope.apiLongitud
    }
    let R = 6378137; // Earth’s mean radius in meter
    let dLat = rad(p2.lat - p1.lat);
    let dLong = rad(p2.lng - p1.lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
      Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    console.log(d);
    $scope.distancia = d;
  };


  navigator.geolocation.getCurrentPosition(onSuccessGeo, onErrorGeo);
}]);
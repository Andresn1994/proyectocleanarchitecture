'use strict';
describe("Testing general", () => {
  beforeEach(module('app'));
  let $controller;
  let controller;
  let $scope = {};
  let $http;

  beforeEach(() => {
    inject(($controller, $rootScope, _$http_) => {
      $scope = $rootScope.$new();
      $http = _$http_;
      $controller("sumaController", {
        $scope: $scope,
        $http: $http
      });
    });
  });

  it("Should sum two numbers", () => {
    $scope.numeroUno = 2;
    $scope.numeroDos = 3;
    let resultado = $scope.suma();
    expect(resultado).toBe(5)
  });

  it("Should call an API", async () => {
    let data = JSON.stringify( {suma_int: 0})
    spyOn($http, "post").and.returnValue(Promise.resolve('successful'))
    await $scope.callAPI();
    expect($http.post).toHaveBeenCalledWith('http://localhost:3001/suma', data)
    expect($scope.valid).toEqual('Exito');
  });

  it("Should fail call an API", async () => {
    let data = JSON.stringify( {suma_int: 0})
    spyOn($http, "post").and.returnValue(Promise.reject('error'))
    await $scope.callAPI();
    expect($http.post).toHaveBeenCalledWith('http://localhost:3001/suma', data)
    expect($scope.valid).toEqual('Error');
  });


});